#pragma once
#include<iostream>
#include<string.h>

using namespace std;

class HasConsoleOperations {
	virtual void ConsoleWrite() = 0;
	virtual void ConsoleRead() = 0;
};

class Masina:HasConsoleOperations {

	char MarcaMasina[20];
	char ModelMasina[20];
	char NumarMasina[10];
public:
	Masina(const char Marca[20], const char Model[20], const char Numar[10])
	{
		strcpy_s(MarcaMasina,Marca);
		strcpy_s(ModelMasina,Model);
		strcpy_s(NumarMasina,Numar);
	}
	Masina()
	{
		strcpy_s(MarcaMasina, "-");
		strcpy_s(ModelMasina, "-");
		strcpy_s(NumarMasina, "-");
	}
	virtual void ConsoleWrite() {
		cout << "Masinuta: " << MarcaMasina << " " <<ModelMasina << " " <<NumarMasina << endl;
	}

	virtual void ConsoleRead() {
		char Marca[20];
		char Model[20];
		char Numar[10];
		cout << "Introdu marca: " << endl;
		cin >> Marca;
		cout << "Introdu modelul: " << endl;
		cin >> Model;
		cout << "Introdu numar: " << endl;
		cin >> Numar;
		strcpy_s(MarcaMasina, Marca);
		strcpy_s(ModelMasina, Model);
		strcpy_s(NumarMasina, Numar);
	}
};

class DetaliiMasina :Masina {
	int Motor;
	int NrLocuri;
	char Serie[20];
	char Combustibil[20];
public:
	DetaliiMasina(const int motor, const int locuri, const char serie[20], const char combustibil[20])
	{
		Motor = motor;
		NrLocuri = locuri;
		strcpy_s(Serie, serie);
		strcpy_s(Combustibil, combustibil);
	}
	DetaliiMasina()
	{
		Motor = 0;
		NrLocuri = 0;
		strcpy_s(Serie, "-");
		strcpy_s(Combustibil, "-");

	}
	virtual void ConsoleWrite() {
		cout << "Detalii pt masinuta: " << Motor << " " << NrLocuri << " " << Serie << " " << Combustibil<<endl;
	}

	virtual void ConsoleRead() {
		int motor;
		int locuri;
		char serie[20];
		char combustibil[20];
		cout << "Introdu capacitate motor: " << endl;
		cin >> motor;
		cout << "Introdu numar locuri: " << endl;
		cin >> locuri;
		cout << "Introdu serie: " << endl;
		cin >> serie;
		cout << "Introdu tipul combustibilului: " << endl;
		cin >> combustibil;
		Motor = motor;
		NrLocuri = locuri;
		strcpy_s(Serie, serie);
		strcpy_s(combustibil, combustibil);
	}
	void VerificaTipCombustibil(const char combustibil[20])
	{
		if (strcmp(Combustibil, combustibil) == 0)
			cout << "Poti sa alimentezi linistit" << endl;
		else
			cout << "NU ALIMENTA! STRICI MASINA " << endl;
	}
};

class Detinator :Masina {
	char Prenume[20];
	char Nume[20];
	char Adresa[50];
public:
	Detinator(const char prenume[20], const char nume[20], const char adresa[50])
	{
		strcpy_s(Prenume, prenume);
		strcpy_s(Nume, nume);
		strcpy_s(Adresa, adresa);
	}
	Detinator()
	{
		strcpy_s(Prenume, "-");
		strcpy_s(Nume, "-");
		strcpy_s(Adresa, "-");
	}
	void updateNume(const char nume[20])
	{
		strcpy_s(Nume, nume);
	}
	void updatePrenume(const char prenume[20])
	{
		strcpy_s(Prenume, prenume);
	}
	void updateAdresa(const char adresa[50])
	{
		strcpy_s(Adresa, adresa);
	}
	virtual void ConsoleWrite() {
		cout << "Detalii proprietar: " << Prenume << " " << Nume << " " << Adresa <<  endl;
	}

	virtual void ConsoleRead() {
		char prenume[20];
		char nume[20];
		char adresa[50];

		cout << "Introdu prenume: " << endl;
		cin >> prenume;
		cout << "Introdu nume: " << endl;
		cin >> nume;
		cout << "Introdu adresa: " << endl;
		cin >> adresa;
		strcpy_s(Nume, nume);
		strcpy_s(Prenume, prenume);
		strcpy_s(Adresa, adresa);
	}
};
